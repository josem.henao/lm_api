import uvicorn as uvicorn
from fastapi import FastAPI

from api.router import api_router
from config import API_APP_CONFIG, SERVER_HOST, SERVER_PORT
from transversal.db import Base, db_engine


def create_app() -> FastAPI:
    try:
        _app = FastAPI(**API_APP_CONFIG)
        _app.include_router(api_router, prefix="/api/v1")
        return _app
    except Exception as e:
        raise Exception(f"Error in FastAPI app initialisation => {e}")


lm_app: FastAPI = create_app()


@lm_app.on_event("startup")
async def _startup() -> None:
    Base.metadata.create_all(bind=db_engine)


if __name__ == "__main__":
    uvicorn.run(lm_app, host=SERVER_HOST, port=SERVER_PORT)

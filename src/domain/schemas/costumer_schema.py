from typing import Optional

from pydantic import BaseModel, EmailStr


class CustomerSchema(BaseModel):
    fist_name: str
    middle_name: Optional[str] = None
    last_name: str
    email: EmailStr
    zip_code: int

    def as_dict(self):
        return {
            'fist_name': self.fist_name,
            'middle_name': self.middle_name,
            'last_name': self.last_name,
            'email': self.email,
            'zip_code': self.zip_code
        }


class CustomerGETSchema(CustomerSchema):
    customer_id: int
    county: Optional[str] = None
    state: str

    def as_dict(self):
        return {
            'customer_id': self.customer_id,
            'fist_name': self.fist_name,
            'middle_name': self.middle_name,
            'last_name': self.last_name,
            'email': self.email,
            'zip_code': self.zip_code,
            'county': self.county,
            'state': self.state,
        }

    class Config:
        orm_mode = True

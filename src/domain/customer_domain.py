import requests

from config import ZIP_API_KEY


class CustomerDomain:
    """
    Implements some Business rules as functionalities
    """

    @staticmethod
    def complement_zip_info(zip_code):
        """
        Finds extra info for a ZIP given in the https://www.zipcodeapi.com/API
        :param: zip_code: the zip code to get the extra info
        :return: a dict with the extra info
            {
                'zip_code': 94203,
                'county': 'Sacramento',
                'state': 'California'
            }
        """
        zip_api_endpoint = f"https://www.zipcodeapi.com/rest/{ZIP_API_KEY}/info.json/{zip_code}/degrees"
        response = requests.get(zip_api_endpoint).json()
        return {
            'zip_code': zip_code,
            'county': response.get('county', None),
            'state': response.get('state', None)
        }

from typing import List, Optional

from sqlalchemy import select

from domain.customer_domain import CustomerDomain
from domain.models.customer_model import CustomerModel
from domain.schemas.costumer_schema import CustomerSchema, CustomerGETSchema


class CustomerService:
    @staticmethod
    async def create_customer(customer: CustomerSchema, db_session) -> CustomerGETSchema:
        new_customer: dict = customer.as_dict()
        new_customer.update(
            CustomerDomain.complement_zip_info(
                new_customer.get('zip_code')
            )
        )
        customer_model = CustomerModel(**new_customer)
        db_session.add(customer_model)
        db_session.commit()
        return customer_model

    @staticmethod
    async def list_customers(limit: int, db_session) -> List[CustomerGETSchema]:
        result = db_session.execute(select(CustomerModel).limit(limit)).fetchall()
        return list(map(lambda x: x[0].as_dict(), result))

    @staticmethod
    async def get_customer_by_id(customer_id: int, db_session) -> Optional[CustomerGETSchema]:
        customer = db_session.get(CustomerModel, customer_id)
        if customer:
            return customer.from_orm(customer)
        else:
            return None

from sqlalchemy import Column, Integer, String

from transversal.db import Base


class CustomerModel(Base):
    __tablename__ = 'customers'

    customer_id = Column(Integer, primary_key=True, index=True)
    fist_name = Column(String(120), nullable=False)
    middle_name = Column(String(120), nullable=True)
    last_name = Column(String(120), nullable=False)
    email = Column(String(120), index=True, unique=True)
    zip_code = Column(String(10), nullable=False)
    county = Column(String(120), nullable=True)
    state = Column(String(120), nullable=False)

    def __repr__(self):
        return f"CustomerModel(" \
               f"customer_id: {self.customer_id}, " \
               f"fist_name: {self.fist_name}, " \
               f"email : {self.email}, " \
               f"zip_code: {self.zip_code})"

    def as_dict(self):
        return {
            'customer_id': self.customer_id,
            'fist_name': self.fist_name,
            'middle_name': self.middle_name,
            'last_name': self.last_name,
            'email': self.email,
            'zip_code': self.zip_code,
            'county': self.county,
            'state': self.state,
        }

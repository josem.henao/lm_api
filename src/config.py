import os

API_APP_CONFIG = {
    'title': 'Launch Mobility Customers ZIP API',
    'description': 'The Launch Mobility ZIP API in a modern API application',
    'debug': True,
    'version': '0.1',
}

SERVER_HOST = '127.0.0.1'
SERVER_PORT = 8000

DB_ENGINE = os.getenv('DB_ENGINE', 'postgresql')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_SCHEMA = os.getenv('DB_SCHEMA')
DATABASE_URI = f"{DB_ENGINE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_SCHEMA}"

ZIP_API_KEY = os.getenv('ZIP_API_KEY')

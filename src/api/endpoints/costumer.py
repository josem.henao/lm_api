from typing import List

from fastapi import APIRouter, Depends
from starlette.status import HTTP_404_NOT_FOUND
from domain.schemas.costumer_schema import CustomerGETSchema, CustomerSchema
from domain.services.customer_service import CustomerService
from transversal.db import get_session

customer_router = APIRouter()


@customer_router.get("/all", response_model=List[CustomerGETSchema])
async def list_customers(limit=10, db_session=Depends(get_session)):
    return await CustomerService.list_customers(limit, db_session)


@customer_router.get("/{customer_id}", response_model=CustomerGETSchema)
async def get_customer(customer_id: int, db_session=Depends(get_session)):
    response =  await CustomerService.get_customer_by_id(customer_id, db_session)
    Response(status_code=HTTP_404_NOT_FOUND)



@customer_router.post("/", response_model=CustomerGETSchema)
async def create_customer(customer: CustomerSchema, db_session=Depends(get_session)):
    return await CustomerService.create_customer(customer, db_session)

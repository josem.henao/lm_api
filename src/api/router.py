from fastapi import APIRouter

from src.api.endpoints.costumer import customer_router

api_router = APIRouter()
api_router.include_router(customer_router, prefix="/customers", tags=["Customers"])

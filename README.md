# About the API

![UrbanPilot General Architecture](UrbanPilot_Architecture.png)

The Launch Mobility API is a RestAPI implemented on top of the FastAPI web framework. This RestAPI ensures the creation, validations
and persistence of the customers in the Launch Mobility UrbanPilot service.

To run this API you must ensure to install the requirements. just run on the project root directory:
```shell
pipenv install
```

Make sure to previously install pipenv using `pip`
## Services

This RestAPI exposes some services through some endpoints which are always relative to the base API endpoint. Make sure 
to export the following environment variables before running:
- Data Base configuration:
  - `DB_ENGINE`: The Data Base engine to use. Default value is _*postgresql*_
  - `DB_USER`: The Database username
  - `DB_PASSWORD`: The database password for the username defined
  - `DB_HOST`: The url to the database. if local use _*localhost*_
  - `DB_PORT`: The database port
  - `DB_SCHEMA`: The database schema
- [ZIP CODE API](https://www.zipcodeapi.com/): You must register and get the API KEY:
  - `ZIP_API_KEY`: 
### Base API Endpoint

```
http://<host>:8080/api/v1/
```

### Available services

#### Customers

- `GET customers/all` Returns a list of all the customers in the DB. You can paginate using the extra query params.
  e.g `?limit=2`'

  ```json
  [
    {
      "customer_id": 100,
      "fist_name": "Johann",
      "last_name": "Strauss",
      "email": "johann.strauss@classic.com",
      "zip_code": 54321
    },
    {
      "customer_id": 101,
      "fist_name": "Achille",
      "middle_name": "Claude",
      "last_name": "Debussy",
      "email": "claude.debussy@classic.com",
      "zip_code": 13542
    }
  ]
  ```

- `GET customers/<customer_id>` Returns the information related to a specific customer_id. e.g: 
`http://<host>:8080/api/v1/customers/101`
```json
  {
      "customer_id": 101,
      "fist_name": "Achille",
      "middle_name": "Claude",
      "last_name": "Debussy",
      "email": "claude.debussy@classic.com",
      "zip_code": 13542
  }
```
- `POST customers/` Sends a customer info to store in the DB. You must provide a valid json in the body of the post
  request:

    ```json
    {
      "fist_name": "Wolfgang",
      "middle_name": "Amadeus",
      "last_name": "Mozart",
      "email": "wolfgang.mozart@classic.com",
      "zip_code": 12345
    }
    ```

  All the fields are Required except for `middle_name` which is optional

### Documentation

One of the differentiating characteristics of FastAPI is the integration it has with the documentation of the exposed 
endpoints. we have two types of documentation. One through [Redoc](https://github.com/Redocly/redoc). 
Which shows us in a simple and concrete way the exposed endpoints as well as the scheme of the data that it receives 
or returns, and the possible response states. Check this documentation under the endpoint `http://<api_host>:<api_port>/redoc/`

We also have a second option for [Swagger](https://swagger.io/) type documentation. This has provided us with the 
definition of the endpoints, a brief description of what can be done with each endpoint; it also gives us the schemas 
that each endpoint receives, finally we can directly test the endpoints directly from this web documentation. You can 
check this functionality under the `http://<api_host>:<api_port>/docs/`endpoint.